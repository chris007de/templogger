/*
 * SD card
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 10
 *
 * DS1302
 ** CE pin    -> Arduino Digital 2
 ** I/O pin   -> Arduino Digital 3
 ** SCLK pin  -> Arduino Digital 4
 */

#include <SerialCommand.h>
#include <SD.h>
#include <dht11.h>
#include <DS1302.h>
#include <EEPROM.h>

#define ADDR_LOGINTERVAL     0
#define ADDR_TEMPOFFSETDIR   1
#define ADDR_TEMPOFFSETVAL   2

#define BRIGHTNESSPIN A5
#define DHT11PIN 5
const int chipSelect = 10;

/*
 * Settings
 */
char* sLogFilename = "datalog.csv";
unsigned char cLogInterval     = 1;
unsigned char cTempOffsetDir   = 0; /* 0 down, 1 up */
unsigned char cTempOffsetValue = 0;

/*
 * Time 
 */
DS1302 rtc(2, 3, 4);
Time t;
Time t_old;

/*
 * Temperature and Humidity
 */
dht11 DHT11;
unsigned int iTemperature = 0;
unsigned int iHumidity    = 0;

/*
 * Brightness
 */
float dBrightness = 0;

/*
 * Operative Objects
 */
SerialCommand sCmd;
File dataFile;
boolean bEdgeMinute = false;
boolean bFileLocked = false;


void setup()
{ 
  Serial.begin(115200);
  Serial.println("TempLogger v1");  
  Serial.println("type 'help' for information"); 
  
  // Setup callbacks for SerialCommand commands
  sCmd.addCommand("help",    showHelp);
  sCmd.addCommand("showValues",    showValues);
  sCmd.addCommand("getStoredData",  getStoredData);
  sCmd.addCommand("clearStoredData",  clearStoredData);
  sCmd.addCommand("setLogInterval",  setLogInterval);
  sCmd.addCommand("setTempOffset", setTempOffset);
  sCmd.addCommand("setTime",  setRTCTime);
  sCmd.setDefaultHandler(unrecognized);      // Handler for command that isn't matched  (says "What?")
  
  
  /* Set RTC to running mode */
  rtc.halt(false);
  
  /* get settings from EEPROM */
  cLogInterval     = EEPROM.read(ADDR_LOGINTERVAL);
  cTempOffsetDir   = EEPROM.read(ADDR_TEMPOFFSETDIR);
  cTempOffsetValue = EEPROM.read(ADDR_TEMPOFFSETVAL);
  
  /* Initialize SD Card */
  Serial.print("Initializing SD card...");
  pinMode(10, OUTPUT);  
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  
  Serial.println("successful.");
  Serial.println();
  
  readSensors();
}

void loop()
{
  sCmd.readSerial();
  
  // Time
  t = rtc.getTime();
  
  /* Time edge-detection */
  if( t.min != t_old.min )
    bEdgeMinute = true;
  else
    bEdgeMinute = false; 
  
  /* Write to log every x minutes */
  if( !(t.min % cLogInterval) && 0 == t.sec && bEdgeMinute )
  {
    readSensors();
    logToSD();
  }

  t_old = t;
 }
 
 void readSensors() {
    // Temperature and humidity
    DHT11.read(DHT11PIN);
    if( 0 == cTempOffsetDir )
      iTemperature = DHT11.temperature - cTempOffsetValue;
    else if ( 1 == cTempOffsetDir )
      iTemperature = DHT11.temperature + cTempOffsetValue;  
    
    iHumidity = DHT11.humidity;  
    
    // Brightness
    dBrightness = (float)analogRead( BRIGHTNESSPIN );
    dBrightness = 1023-dBrightness;
    dBrightness = dBrightness*0.09765625; // 100/1023    
 }

void showHelp() {
  Serial.println();
  Serial.println("Available Commands:");
  Serial.println("* showValues");
  Serial.println("* getStoredData");
  Serial.println("* clearStoredData");
  Serial.println("* setLogInterval [minutes 0..59]");
  Serial.println("* setTempOffset [+/-] [0..255]");
  Serial.println("* setRTCTime [year] [mon] [day] [hour] [min] [sec]");
  Serial.println();
}


void showValues() {
  Serial.println();
  Serial.print(t.date, DEC);
  Serial.print(".");
  Serial.print(t.mon, DEC);
  Serial.print(".");
  Serial.print(t.year, DEC);
  Serial.print(" - ");
  Serial.print(t.hour, DEC);
  Serial.print(":");
  Serial.print(t.min, DEC);
  Serial.print(":");
  Serial.print(t.sec, DEC);
  Serial.println();
  
  Serial.print("Temperature (oC): ");
  Serial.println(iTemperature); 
  Serial.print("Temperature Offset: ");
  if( 1 == cTempOffsetDir )
    Serial.print("+");
  else
    Serial.print("-");
  Serial.println(cTempOffsetValue);
  Serial.print("Humidity (%): ");
  Serial.println(iHumidity); 
  Serial.print("Brightness (%): ");
  Serial.println((int)dBrightness);
  Serial.print("Logging interval (minutes): ");
  Serial.println(cLogInterval);
  Serial.println();
}


void getStoredData() {
  if(!bFileLocked)
  {
    bFileLocked = true;
    Serial.println("Reading data");
    dataFile = SD.open(sLogFilename);
    
    if (dataFile) {
      while (dataFile.available()) {
        Serial.write(dataFile.read());
      }
      dataFile.close();
    }  
    // if the file isn't open, pop up an error:
    else {
      Serial.print("error opening logfile");
      Serial.println(sLogFilename);
    } 
    Serial.println();
    bFileLocked = false;
  }
}


void clearStoredData() {
  if(!bFileLocked)
  {
    bFileLocked = true;
  SD.remove(sLogFilename);
  Serial.println("Cleared the stored data");  
    bFileLocked = false;
  }
  initSD();
}


void setRTCTime() {
  char *arg;
  unsigned int h_,m_,s_,day_,month_,year_;
  arg = sCmd.next();    
  if (arg != NULL) {    
    year_ = atoi(arg);
  } else return;
  arg = sCmd.next();    
  if (arg != NULL) {    
    month_ = atoi(arg);
  } else return;
  arg = sCmd.next();
  if (arg != NULL) {
    day_ = atoi(arg);
  } else return;
  arg = sCmd.next();
  if (arg != NULL) {
    h_ = atoi(arg);
  } else return;
  arg = sCmd.next();
  if (arg != NULL) {
    m_ = atoi(arg);
  } else return;
  arg = sCmd.next();
  if (arg != NULL) {
    s_ = atoi(arg);
  } else return;  
  
  rtc.setDate(day_,month_,year_);
  rtc.setTime(h_,m_,s_);
}


void setLogInterval() {
  char *arg;
  arg = sCmd.next(); 
  if (arg != NULL) { 
    cLogInterval = atoi(arg);
    EEPROM.write(ADDR_LOGINTERVAL, cLogInterval);
  }
}


void setTempOffset() {
  char *arg;
  arg = sCmd.next(); 
  if (arg != NULL) {
    if( '+' == arg[0] )
      cTempOffsetDir = 1;
    else if ( '-' == arg[0] )
      cTempOffsetDir = 0;
      
    EEPROM.write(ADDR_TEMPOFFSETDIR, cTempOffsetDir);      
  }
  
  arg = sCmd.next(); 
  if (arg != NULL) { 
    cTempOffsetValue = atoi(arg);
    EEPROM.write(ADDR_TEMPOFFSETVAL, cTempOffsetValue);
  }
}


void unrecognized(const char *command) {
  showHelp();
}

/* 
 * SD functions
 */
 void initSD() {
   if( !bFileLocked)
   {
     bFileLocked = true;
     dataFile = SD.open(sLogFilename, FILE_WRITE);
     if (dataFile) {
      dataFile.print("date;");
      dataFile.print("temp;");
      dataFile.print("hum;");
      dataFile.print("bright");
      dataFile.println();
      dataFile.close();
     }
     bFileLocked = false;
   }
 }
 
void logToSD() {
  if( !bFileLocked)
  {
    bFileLocked = true;
    Serial.println("Logging data to SD");
    
    /* Open File */
    dataFile = SD.open(sLogFilename, FILE_WRITE);
  
    // if the file is available, write to it:
    if (dataFile) {
      dataFile.print("\"");
      dataFile.print(t.date);
      dataFile.print(".");
      dataFile.print(t.mon);
      dataFile.print(".");      
      dataFile.print(t.year);
      dataFile.print(" ");
      dataFile.print(t.hour);
      dataFile.print(":");
      dataFile.print(t.min);
      dataFile.print("\"");
      // Logged Data
      dataFile.print(";");
      dataFile.print(DHT11.temperature);
      dataFile.print(";");
      dataFile.print(DHT11.humidity);
      dataFile.print(";");
      dataFile.print((int)dBrightness);
      dataFile.println();
      dataFile.close();
    }  
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening logfile");
    } 
    bFileLocked = false;
  }
}
